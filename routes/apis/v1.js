const clockController = require('../../controllers/clock')
const express = require('express')

let router = express.Router()
router.use('/clock', clockController)

module.exports = router