const express = require('express')
let Clock = require('../models/clock')

let getDates = async (req, res, next) => {
    try {

        let dates = await Clock.find({})

        if (dates.length > 0) {
            return res.status(200).json({
                'data': dates
            })
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No dates found in the system'
        })

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'Something went wrong, Please try again'
        })
    }
}

let getTime = async (req, res, next) => {
    try {
        
        let tempHour = {
            hour: Math.round(Math.random()*12),
            minute: Math.round(Math.random()*60)
        }

        let newHour = await Clock.create(tempHour)

        if (newHour) {
            return res.status(200).json({
                'data': newHour
            })
        } else {
            throw new Error('something went wrong')
        }

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'Something went wrong, Please try again'
        })
    }
}

module.exports = {
    getDates: getDates,
    getTime: getTime
}