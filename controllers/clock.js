const express = require('express')
const clockService = require('../services/clock')
let router = express.Router()

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8080")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  })

router.get('/', clockService.getDates)

router.get('/time', clockService.getTime)

module.exports = router
