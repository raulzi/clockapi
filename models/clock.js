let mongoose = require('mongoose')
let Schema = mongoose.Schema

var clockSchema = new Schema({
    hour: {
        type: Number,
        min: 0,
        max: 23
    },
    minute: {
        type: Number,
        min: 0,
        max: 59
    },
    creationDate: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Clock', clockSchema)